use std::fs::File;
use std::io::prelude::*;
enum Instru{
    F,L,R,
}

struct Robot {
    x: i32,
    y: i32,
    orientation: String,
}
struct Terrain {
    xmax: i32,
    ymax: i32,
}
fn main() {

      let mut file = File::open("..\two_robots.txt").expect("Erreur"); // recupere les donnees de fichier two robots

    let mut content = String::new();
    file.read_to_string(&mut content).expect("Erreur"); // metter les valeurs dans une chaine de carateres 
    let mut i=0;
    let mut tab=[]; // un tableau pour les lignes 
    for line in content.lines(){
        tab[i] = line;
        i=i+1;
    }
    let t: Vec<&str> = tab[0].split(" ").collect(); // recupere les valeurs de terrain 


    let mut terrain = Terrain{
        xmax: t[0].parse().unwrap(),
        ymax: t[1],parse().unwrap(),
    };
    let r1: Vec<&str> = tab[1].split(" ").collect(); // recupere les valeurs de robot 1 
    let r2: Vec<&str> = tab[3].split(" ").collect(); // recupere les valeurs de robot 1

    let instruction = vec![Instru::F,Instru::L,Instru::F];
    let mut or1: String; // orientation de robot 1

    let mut or2: String; // orientation de robot 2

    match r1[3]{ // recupere la valeur d'orientation de robot 1
        "N"=>{
            or1: "North".to_string();
        }
        "S"=>{
            or1: "South".to_string();
        }
        "E"=>{
            or1: "Est".to_string();
        }
        "W"=>{
            or1: "West".to_string();
        }
    }
    match r2[3]{ // recupere la valeur d'orientation de robot 1
        "N"=>{
            or1: "North".to_string();
        }
        "S"=>{
            or1: "South".to_string();
        }
        "E"=>{
            or1: "Est".to_string();
        }
        "W"=>{
            or1: "West".to_string();
        }

        let instruction = vec![Instru::F,Instru::L,Instru::F];
    let mut robot = Robot { // initialiser les valeurs de robot 1
        x: r1[0].parse().unwrap(),
        y: r1[1].parse().unwrap(),
        orientation: or1,
    };

    let instruction1 = vec![Instru::F,Instru::R,Instru::F];
    let mut robot1 = Robot { // initialiser les valeurs de robot 2
         x: r2[0].parse().unwrap(),
         y: r2[1].parse().unwrap(),
        orientation: or2,
    };

    println!("Les coordonnées des robot 0 initial sont X = {} et Y = {} et l'orientation est {}",robot.x , robot.y, robot.orientation);
    println!("Les coordonnées des robot 1 initial sont X = {} et Y = {} et l'orientation est {}",robot1.x , robot1.y, robot1.orientation);
for i in 0..3 { // les deplacement 
        match instruction[i] {
            Instru::F => {
            if robot.orientation == "North".to_string() {
                if robot.y + 1 == robot1.y && robot.x == robot1.x{ // collision
                    println!("Robot 0 collision en ({},{})",robot1.x , robot1.y);
                }else{
               robot.y = robot.y + 1;
                }
            }  
            if robot.orientation == "South".to_string(){
                if robot.y - 1 == robot1.y && robot.x == robot1.x{ // collision
                    println!("Robot 0 collision en ({},{})",robot1.x , robot1.y);
                }else{
             robot.y = robot.y - 1 ;}
            }
            if robot.orientation == "West".to_string(){
                if robot.x - 1 == robot1.x && robot.y == robot1.y{// collision
                    println!("Robot 0 collision en ({},{})",robot1.x , robot1.y);
                }else{
                robot.x = robot.x - 1 ;}
            }
            if robot.orientation == "Est".to_string(){
                if robot.x + 1 == robot1.x && robot.y == robot1.y{// collision
                    println!("Robot 0 collision en ({},{})",robot1.x , robot1.y);
                }else{
                robot.x = robot.x + 1 ;}
            }

        }
           Instru:: L=> {
               if robot.orientation == "North".to_string(){
                   robot.orientation = "West".to_string();}
                   else if robot.orientation == "West".to_string(){
                    robot.orientation = "South".to_string();}
                    else if robot.orientation == "South".to_string(){
                        robot.orientation = "Est".to_string();}
                     else if robot.orientation == "Est".to_string(){
                            robot.orientation = "North".to_string();} 
               }
           
           Instru:: R=> {
            if robot.orientation == "North".to_string(){
                robot.orientation = "Est".to_string();}
                else if robot.orientation == "Est".to_string(){
                 robot.orientation = "South".to_string();}
                else if robot.orientation == "South".to_string(){
                     robot.orientation = "West".to_string();}
                    else if robot.orientation == "West".to_string(){
                         robot.orientation = "North".to_string();} 
            }
        }
    
    }    
    for i in 0..3 {
        match instruction1[i] {
            Instru::F => { // les deplacement 
           if robot1.orientation == "North".to_string() {
                if robot.y + 1 == robot1.y && robot.x == robot1.x{ // collision
                    println!("Robot 1 collision en ({},{})",robot.x , robot.y);
                }else{
               robot1.y = robot1.y + 1;
                }
            }  
            if robot1.orientation == "South".to_string(){
                if robot.y - 1 == robot1.y && robot.x == robot1.x{ // collision
                    println!("Robot 1 collision en ({},{})",robot.x , robot.y);
                }else{
             robot1.y = robot1.y - 1 ;}
            }
            if robot1.orientation == "West".to_string(){
                if robot.x - 1 == robot1.x && robot.y == robot1.y{ // collision
                    println!("Robot 1 collision en ({},{})",robot.x , robot.y);
                }else{
                robot1.x = robot1.x - 1 ;}
            }
            if robot1.orientation == "Est".to_string(){
                if robot.x + 1 == robot1.x && robot.y == robot1.y{ // collision
                    println!("Robot 1 collision en ({},{})",robot.x , robot.y);
                }else{
                robot1.x = robot1.x + 1 ;}
            }
        }
           Instru:: L=> {
               if robot1.orientation == "North".to_string(){
                   robot1.orientation = "West".to_string();}
                   else if robot1.orientation == "West".to_string(){
                    robot1.orientation = "South".to_string();}
                    else if robot1.orientation == "South".to_string(){
                        robot1.orientation = "Est".to_string();}
                     else if robot1.orientation == "Est".to_string(){
                            robot1.orientation = "North".to_string();} 
               }
           
           Instru:: R=> {
            if robot1.orientation == "North".to_string(){
                robot1.orientation = "Est".to_string();}
                else if robot1.orientation == "Est".to_string(){
                 robot1.orientation = "South".to_string();}
                else if robot1.orientation == "South".to_string(){
                     robot1.orientation = "West".to_string();}
                    else if robot1.orientation == "West".to_string(){
                         robot1.orientation = "North".to_string();} 
            }
        }
    
    }
println!("Final");
println!("Les coordonnées des robot 0 sont X = {} et Y = {} et l'orientation est {}",robot.x , robot.y , robot1.orientation);
println!("Les coordonnées des robot 1 sont X = {} et Y = {} et l'orientation est {}",robot1.x , robot1.y , robot1.orientation);
}
